const { admin, db } = require('../util/admin');

const config = require('../util/config');

const firebase = require('firebase');
firebase.initializeApp(config);

const {
  validateSignupData,
  validateLoginData,
} = require('../util/validators');

// Sign users up
exports.signup = (req, res) => {
  const newUser = {
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.password,
    name: req.body.name,
    regNo: req.body.regNo,
    phoneNo: req.body.phoneNo
  };

  const { valid, errors } = validateSignupData(newUser);
  if (!valid) return res.status(200).json({error: "Make sure no field is empty"});

  let token, userId;
  db.doc(`/users/${newUser.regNo}`).get().then((doc) => {
      if (doc.exists) {
        return res.status(200).json({ regNo: 'this is already registered' });
      } else {
        return firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password);
      }
    }).then((data) => {
      userId = data.user.uid;
      return data.user.getIdToken();
    }).then((idToken) => {
      token = idToken;
      const userCredentials = {
        name:newUser.name,
        email: newUser.email,
        createdAt: new Date().toISOString(),
        regNo: newUser.regNo,
        phoneNo: newUser.phoneNo,
        userId
      };
      return db.doc(`/users/${newUser.regNo}`).set(userCredentials);
    }).then(() => {
      return res.status(200).json({ token });
    }).catch((err) => {
      if (err.code === 'auth/email-already-in-use') {
        return res.status(200).json({ error: 'Email is already is use' });
      } else
        return res.status(200).json({ error: 'Something went wrong, please try again' });
    });
};
// Log user in
exports.login = (req, res) => {
  console.log(req.body)
  const user = {
    email: req.body.email,
    password: req.body.password
  };

  const { valid, errors } = validateLoginData(user);

  if (!valid) return res.status(200).json(errors);

  firebase.auth().signInWithEmailAndPassword(user.email, user.password)
    .then((data) => {
      console.log(data.user) 
      return data.user.getIdToken();
    })
    .then((token) => {
      return res.json({ token });
    })
    .catch((err) => {
      console.error(err);
      // auth/wrong-password
      // auth/user-not-user
      return res
        .status(200)
        .json({ error: 'Wrong credentials, please try again' });
    });
};